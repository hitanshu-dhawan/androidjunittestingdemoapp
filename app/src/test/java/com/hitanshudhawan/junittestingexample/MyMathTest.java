package com.hitanshudhawan.junittestingexample;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by hitanshu on 18/4/18.
 */

public class MyMathTest {

    @Test
    public void absTest1() {
        assertEquals(6,MyMath.abs(6));
    }

    @Test
    public void absTest2() {
        assertEquals(6,MyMath.abs(-6));
    }

    @Test
    public void absTest3() {
        assertEquals(0,MyMath.abs(0));
    }

    @Test
    public void additionTest1() {
        assertEquals(10,MyMath.add(6,4));
    }

    @Test
    public void additionTest2() {
        assertEquals(-10,MyMath.add(-6,-4));
    }

    @Test
    public void additionTest3() {
        assertEquals(-2,MyMath.add(-6,4));
    }

    @Test
    public void subtractionTest1() {
        assertEquals(2,MyMath.subtract(6,4));
    }

    @Test
    public void subtractionTest2() {
        assertEquals(-2,MyMath.subtract(-6,-4));
    }

    @Test
    public void subtractionTest3() {
        assertEquals(-10,MyMath.subtract(-6,4));
    }

    @Test
    public void multiplicationTest1() {
        assertEquals(24,MyMath.multiply(6,4));
    }

    @Test
    public void multiplicationTest2() {
        assertEquals(24,MyMath.multiply(-6,-4));
    }

    @Test
    public void multiplicationTest3() {
        assertEquals(-24,MyMath.multiply(-6,4));
    }

    @Test
    public void divisionTest1() {
        assertEquals(2,MyMath.divide(8,4));
    }

    @Test
    public void divisionTest2() {
        assertEquals(2,MyMath.divide(-8,-4));
    }

    @Test
    public void divisionTest3() {
        assertEquals(-2,MyMath.divide(-8,4));
    }

    @Test
    public void floorTest1() {
        assertEquals(6,MyMath.floor(6.78));
    }

    @Test
    public void floorTest2() {
        assertEquals(-7,MyMath.floor(-6.78));
    }

    @Test
    public void floorTest3() {
        assertEquals(6,MyMath.floor(6));
    }

    @Test
    public void floorTest4() {
        assertEquals(-6,MyMath.floor(-6));
    }

    @Test
    public void floorTest5() {
        assertEquals(0,MyMath.floor(0));
    }

    @Test
    public void ceilTest1() {
        assertEquals(7,MyMath.ceil(6.78));
    }

    @Test
    public void ceilTest2() {
        assertEquals(-6,MyMath.ceil(-6.78));
    }

    @Test
    public void ceilTest3() {
        assertEquals(6,MyMath.ceil(6));
    }

    @Test
    public void ceilTest4() {
        assertEquals(-6,MyMath.ceil(-6));
    }

    @Test
    public void ceilTest5() {
        assertEquals(0,MyMath.ceil(0));
    }

}

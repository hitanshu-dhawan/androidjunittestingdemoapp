package com.hitanshudhawan.junittestingexample;

import android.content.SharedPreferences;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by hitanshu on 18/4/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class SharedPreferencesTest {

    @Mock
    SharedPreferences sharedPreferences;

    @Test
    public void sharedPreferencesTest() {

        // mock
        when(sharedPreferences.getInt("number1",0)).thenReturn(25);
        when(sharedPreferences.getInt("number2",0)).thenReturn(50);

        int a = sharedPreferences.getInt("number1",0);
        int b = sharedPreferences.getInt("number2",0);
        assertEquals(75,MyMath.add(a,b));
    }

}

package com.hitanshudhawan.junittestingexample;

/**
 * Created by hitanshu on 18/4/18.
 */

public class MyMath {

    public static int abs(int a) {
        return (a >= 0) ? a : -a;
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static int subtract(int a, int b) {
        return a - b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static int divide(int a, int b) {
        return a / b;
    }

    public static int floor(double a) {
        if (a >= 0) {
            return (int) a;
        } else {
            return ((int) a == a) ? (int) a : (int) (a - 1);
        }
    }

    public static int ceil(double a) {
        if (a >= 0) {
            return ((int) a == a) ? (int) a : (int) (a + 1);
        } else {
            return (int) a;
        }
    }

}
